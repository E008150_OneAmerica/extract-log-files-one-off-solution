﻿namespace ExtractLogFiles
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLogFileExtract = new System.Windows.Forms.Label();
            this.tbLogFileExtract = new System.Windows.Forms.TextBox();
            this.btnLogFileExtract = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.dgvProcessedLogFile = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcessedLogFile)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLogFileExtract
            // 
            this.lblLogFileExtract.AutoSize = true;
            this.lblLogFileExtract.Location = new System.Drawing.Point(3, 13);
            this.lblLogFileExtract.Name = "lblLogFileExtract";
            this.lblLogFileExtract.Size = new System.Drawing.Size(97, 13);
            this.lblLogFileExtract.TabIndex = 0;
            this.lblLogFileExtract.Text = "Log FIle To Extract";
            // 
            // tbLogFileExtract
            // 
            this.tbLogFileExtract.Location = new System.Drawing.Point(107, 6);
            this.tbLogFileExtract.Name = "tbLogFileExtract";
            this.tbLogFileExtract.Size = new System.Drawing.Size(211, 20);
            this.tbLogFileExtract.TabIndex = 1;
            // 
            // btnLogFileExtract
            // 
            this.btnLogFileExtract.Location = new System.Drawing.Point(325, 3);
            this.btnLogFileExtract.Name = "btnLogFileExtract";
            this.btnLogFileExtract.Size = new System.Drawing.Size(30, 23);
            this.btnLogFileExtract.TabIndex = 2;
            this.btnLogFileExtract.Text = ". . .";
            this.btnLogFileExtract.UseVisualStyleBackColor = true;
            this.btnLogFileExtract.Click += new System.EventHandler(this.btnLogFileExtract_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(107, 32);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 3;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // dgvProcessedLogFile
            // 
            this.dgvProcessedLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProcessedLogFile.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProcessedLogFile.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvProcessedLogFile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProcessedLogFile.Location = new System.Drawing.Point(6, 64);
            this.dgvProcessedLogFile.Name = "dgvProcessedLogFile";
            this.dgvProcessedLogFile.Size = new System.Drawing.Size(1047, 481);
            this.dgvProcessedLogFile.TabIndex = 4;
            this.dgvProcessedLogFile.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvProcessedLogFile_ColumnHeaderMouseClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1065, 557);
            this.Controls.Add(this.dgvProcessedLogFile);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.btnLogFileExtract);
            this.Controls.Add(this.tbLogFileExtract);
            this.Controls.Add(this.lblLogFileExtract);
            this.Name = "Form1";
            this.Text = "Extract Log File";
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcessedLogFile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLogFileExtract;
        private System.Windows.Forms.TextBox tbLogFileExtract;
        private System.Windows.Forms.Button btnLogFileExtract;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.DataGridView dgvProcessedLogFile;
    }
}

