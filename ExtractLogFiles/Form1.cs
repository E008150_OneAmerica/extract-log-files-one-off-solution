﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ExtractLogFiles
{
    public partial class Form1 : Form
    {
        string _inputFile = string.Empty;

        DataTable _dtAccountHistory = new DataTable();
        DataTable _dtParticipantInfo = new DataTable();

        public Form1()
        {
            InitializeComponent();
        }

        #region Handle Button Presses
        private void btnLogFileExtract_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdFileToExtract = new OpenFileDialog();

            if (ofdFileToExtract.ShowDialog() == DialogResult.OK) {
                _inputFile = ofdFileToExtract.FileName;
                
                tbLogFileExtract.Text = _inputFile;
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;

            dgvProcessedLogFile.DataSource = ProcessFile(_inputFile);
            for (int _loop = 0; _loop < dgvProcessedLogFile.Columns.Count - 1; _loop++) {
                dgvProcessedLogFile.Columns[_loop].SortMode = DataGridViewColumnSortMode.Programmatic;
            }

            Cursor.Current = Cursors.Default;
        }
        #endregion  

        #region Process a File
        private DataTable ProcessFile(string inputFile)
        {
            //
            // Determine type of file, and send to right processing method
            //

            string _line = string.Empty;

            StreamReader _toProcess = File.OpenText(inputFile);
            _line = _toProcess.ReadLine();
            _toProcess.Close();
            if (_line.Contains("ParticipantInfo Trigger")) {
                return ProcessParticipantInfoFile(inputFile);
            }
            else {
                return ProcessAccountHistoryFile(inputFile);
            }

        }
        #endregion

        #region Process a Participant Info File
        private DataTable ProcessParticipantInfoFile(string inputFile)
        {
            string _line = string.Empty;
            string[] _params;
            char[] _delim = { ':', '\"' };
            DateTime _temp = DateTime.MinValue;

            _partFileType _logFile = new _partFileType();

            //
            // if line contains "START ParticipantInfo Trigger" then
            //   instaniate class for data
            //   decode and parse line
            //   place log entry date/time into class
            //   while line does not containt "END ParticipantInfo Trigger" then
            //     if line contains "Plan ID"
            //       parse and decode Plan ID
            //       place into class
            //     if line contains "Participant ID"
            //       parse and decode Participant ID
            //       place into class
            //   if line contains "END ParticipantInfo Trigger" then
            //     place log entry date/time into class
            //     subtract start log entry time from end entry time and place into class
            //     add to datatable
            //

            _dtParticipantInfo.Columns.Add("StartDateTime");
            _dtParticipantInfo.Columns.Add("EndDateTime");
            _dtParticipantInfo.Columns.Add("PlanID");
            _dtParticipantInfo.Columns.Add("ParticpantID");
            _dtParticipantInfo.Columns.Add("TimeToRun");

            StreamReader _toProcess = File.OpenText(inputFile);
            while ((_line = _toProcess.ReadLine()) != null) {
                if (_line.Contains("START ParticipantInfo Trigger")) {
                    _logFile = new _partFileType();
                    _params = _line.Split(_delim, StringSplitOptions.RemoveEmptyEntries);
                    DateTime.TryParse(_params[0] + ":" + _params[1] + ":" + _params[2], out _temp);
                    _logFile.StartDateTime = _temp.ToString();
                    while ((_line = _toProcess.ReadLine()) != null) {
                        if (_line.Contains("Plan ID")) {
                            _params = _line.Split(_delim, StringSplitOptions.RemoveEmptyEntries);
                            _logFile.PlanID = _params[6];
                        } else if (_line.Contains("Participant ID")) {
                            _params = _line.Split(_delim, StringSplitOptions.RemoveEmptyEntries);
                            _logFile.PartID = _params[6];
                        } else if (_line.Contains("END ParticipantInfo Trigger")) {
                            _params = _line.Split(_delim, StringSplitOptions.RemoveEmptyEntries);
                            DateTime.TryParse(_params[0] + ":" + _params[1] + ":" + _params[2], out _temp);
                            _logFile.EndDateTime = _temp.ToString();
                            _logFile.TimeToRun = Convert.ToDateTime(_logFile.EndDateTime) - Convert.ToDateTime(_logFile.StartDateTime);
                            break;
                        }
                    }
                    _dtParticipantInfo.Rows.Add(_logFile.StartDateTime, _logFile.EndDateTime, _logFile.PlanID, _logFile.PartID, _logFile.TimeToRun.ToString());
                }
            }
            return _dtParticipantInfo;
        }
        #endregion

        #region Process an Account History File
        private DataTable ProcessAccountHistoryFile(string inputFile)
        {
            string _line = string.Empty;
            string[] _params;
            char[] _delim = {':', '\"'};
            DateTime _temp = DateTime.MinValue;
            _AccHistFileType _logFile = new _AccHistFileType();;
            bool _hasStartValue = false;
            bool _hasEndValue = false;

            //
            // if line contains "BeginDate: " then
            //   create new log entry
            //   set _hasStartValue = true;
            //   parse and decode entry
            // else if line containts "EndDate: " and _hasStartValue = false then
            //   create new log entry
            //   set _hasEndValue = true;
            //   parse and decode entry
            // else if line contains "EndDate: " and _hasStartValue = true then
            //   set _hasEndValue = true;
            //   parse and decode entry
            // 

            // Build out the datatable structure
            _dtAccountHistory.Columns.Add("Log Entry Start", typeof(string));
            _dtAccountHistory.Columns.Add("Log Entry Stop", typeof(string));
            _dtAccountHistory.Columns.Add("Plan Number", typeof(string));
            _dtAccountHistory.Columns.Add("SSN", typeof(string));
            _dtAccountHistory.Columns.Add("Start Date Requested", typeof(string));
            _dtAccountHistory.Columns.Add("End Date Requested", typeof(string));

            StreamReader _toProcess = File.OpenText(inputFile);
            while ((_line = _toProcess.ReadLine()) != null) {
                if (_line.Contains("BeginDate: ")) {
                    _logFile = new _AccHistFileType();
                    _hasStartValue = true;
                    _params = _line.Split(_delim, StringSplitOptions.RemoveEmptyEntries);
                    if (_params.Count() == 7) {
                        DateTime.TryParse(_params[0] + ":" + _params[1] + ":" + _params[2], out _temp);
                        _logFile.LogEntryStart = _temp.ToString();
                        _logFile.PlanNumber = _params[3];
                        _logFile.SSN = _params[4];
                        _logFile.StartDateRequested = _params[6];
                    }
                } else if (_line.Contains("EndDate: ") && !_hasStartValue) {
                    _dtAccountHistory.Rows.Add(_logFile.LogEntryStart, _logFile.LogEntryStop, _logFile.PlanNumber, _logFile.SSN, _logFile.StartDateRequested, _logFile.EndDateRequested);
                    _logFile = new _AccHistFileType();
                    _params = _line.Split(_delim, StringSplitOptions.RemoveEmptyEntries);
                    if (_params.Count() == 7) {
                        DateTime.TryParse(_params[0] + ":" + _params[1] + ":" + _params[2], out _temp);
                        _logFile.LogEntryStop = _temp.ToString();
                        _logFile.PlanNumber = _params[3];
                        _logFile.SSN = _params[4];
                        _logFile.EndDateRequested = _params[6];
                        _hasEndValue = true;
                    }
                } else if (_line.Contains("EndDate: ") && _hasStartValue) {
                    _params = _line.Split(_delim, StringSplitOptions.RemoveEmptyEntries);
                    if (_params.Count() == 7) {
                        DateTime.TryParse(_params[0] + ":" + _params[1] + ":" + _params[2], out _temp);
                        _logFile.LogEntryStop = _temp.ToString();
                        _logFile.PlanNumber = _params[3];
                        _logFile.SSN = _params[4];
                        _logFile.EndDateRequested = _params[6];
                        _hasEndValue = true;
                    }
                }
                if (_hasStartValue && _hasEndValue) {
                    _dtAccountHistory.Rows.Add(_logFile.LogEntryStart, _logFile.LogEntryStop, _logFile.PlanNumber, _logFile.SSN, _logFile.StartDateRequested, _logFile.EndDateRequested);
                    _logFile = null;
                    _hasStartValue = false;
                    _hasEndValue = false;
                }
            }
            return _dtAccountHistory;
        }
        #endregion

        #region Sort columns
        private void dgvProcessedLogFile_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //statusLabel.Text = "Sorting...";
            this.Cursor = Cursors.WaitCursor;
            for (int i = 0; i < dgvProcessedLogFile.ColumnCount; i++) {

                if (e.ColumnIndex == i) {
                    if (dgvProcessedLogFile.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection != SortOrder.Ascending) {
                        dgvProcessedLogFile.Sort(dgvProcessedLogFile.Columns[e.ColumnIndex], ListSortDirection.Ascending);
                        dgvProcessedLogFile.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                    } else {
                        dgvProcessedLogFile.Sort(dgvProcessedLogFile.Columns[e.ColumnIndex], ListSortDirection.Descending);
                        dgvProcessedLogFile.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                    }
                } else {
                    dgvProcessedLogFile.Columns[i].HeaderCell.SortGlyphDirection = SortOrder.None;
                }
            }
            //statusLabel.Text = "";
            this.Cursor = Cursors.Default;
        }
        #endregion
    }

    #region Class for AccountHistoryLogFileType
    public class _AccHistFileType
    {
        public string LogEntryStart { get; set; }
        public string LogEntryStop { get; set; }
        public string PlanNumber { get; set; }
        public string SSN { get; set; }
        public string StartDateRequested { get; set; }
        public string EndDateRequested { get; set; }
    }
    #endregion

    #region Class for ParticipantInfoFileTYpe
    public class _partFileType
    {
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string PlanID { get; set; }
        public string PartID { get; set; }
        public TimeSpan TimeToRun { get; set; }
    }
    #endregion
}
